import { Component, Input } from "@angular/core";
import * as Survey from "survey-angular";
import * as widgets from "surveyjs-widgets";

import "inputmask/dist/inputmask/phone-codes/phone.js";

widgets.icheck(Survey);
widgets.select2(Survey);
widgets.imagepicker(Survey);
widgets.inputmask(Survey);
widgets.jquerybarrating(Survey);
widgets.jqueryuidatepicker(Survey);
widgets.nouislider(Survey);
widgets.select2tagbox(Survey);
widgets.signaturepad(Survey);
widgets.sortablejs(Survey);
widgets.ckeditor(Survey);
widgets.autocomplete(Survey);
widgets.bootstrapslider(Survey);

@Component({
  selector: "survey",
  template: `<div class="survey-container contentcontainer codecontainer">
                <div id="surveyElement">
                </div>
              </div>
              <div class="survey-container contentcontainer codecontainer">
                <div id="surveyResult">
                </div>
              </div>`
})
export class SurveyComponent {
  @Input()
  set json(value: object) {
    console.log('value ', value)
    console.log('value title', value['title'])
    console.log('value pages', value['pages'])
    const surveyModel = new Survey.Model(value);
    console.log('surveyModel ', surveyModel);
    console.log('surveyModel pagesValue ', surveyModel['pagesValue'])
    console.log('surveyModel pagesValue [0] ', surveyModel['pagesValue'][0])//objet de la 1ere page

    surveyModel.onComplete
      .add(function (result) {
        console.log('JSON.stringify(result.data) :',JSON.parse(JSON.stringify(result.data)))
        document.querySelector('#surveyResult').innerHTML = "result: " + JSON.parse(JSON.stringify(result.data));
      });



    Survey.SurveyNG.render("surveyElement", { model: surveyModel });

    console.log('survey.surveyNG ', Survey.SurveyNG);



  }

  ngOnInit() { }


}
