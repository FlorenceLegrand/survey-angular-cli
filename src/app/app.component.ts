import { Component } from "@angular/core";
import { Placeholder } from "@angular/compiler/src/i18n/i18n_ast";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "app works!";
  json = {
    title: "Formulaire Téléopérateur",
    showProgressBar: "top",
    pages: [
      {//page 1
        name: "Identification entreprise",
        title: "Identification entreprise",
        elements: [
          {
            type: "dropdown",
            name: "Nom de l'entreprise",
            hasOther: true,
            choices: [
              "Danone",
              "La Poste",
              "E.D.F"
            ] 
          },
          {
            type: "dropdown",
            name: "Sélectionner le service concerné",
            hasOther: true,
            choices: [
              "Logistique",
              "Livraison",
              "Administratif"
            ]
          },
          {
            type: "dropdown",
            name: "Sélectionner le responsable",
            hasOther: true,
            choices: [
              "Gérard Durand",
              "Françoise Hénaff",
              "Fabrice Legall"
            ]
          },

        ]
      },

      {//page 2
        name: "Identification conducteur",
        title: "Identification conducteur",

        questions: [
          {
            type: "dropdown",
            name: "Sélectionner le conducteur",
            hasOther: true,
            choices: [
              "Julien Renard",
              "Eloïse Pennec",
              "Renan Le Gall",
              "Olivier Bourgueil"
            ]
          },
          { // champs vérif année de naissance
            type: "label",
            name: "Année de naissance :"

          },

          {
            type: "checkbox",
            name: "Quel permis possédez-vous ?",
            choices: [
              "Permis B",
              "Permis C",
              "Permis D",
              "Permis BE"
            ]
          },
          //champs date obtention permis
        ]
      },

      {//page 3
        name: "Identification véhicule",
        title: "Identification véhicule",
        questions: [

          {
            type: "dropdown",
            name: "Sélectionner le véhicule",
            hasOther: true,
            choices: [
              "Kangoo",
              "Renault trafic",
              "VW transporter",
              "clio II"
            ]
          },
          //champ numéro d'immatriculation
        ]
      },


      {//page 4
        name: "Accident",
        title: "Circonstances conducteur",
        questions: [
          {
            type: "dropdown",
            name: "A quel titre le conducteur conduisait-il ?",
            hasOther: true,
            choices: ["Personnel", "Livraison", "Réunion extérieur"]
          },

          {
            type: "text",
            name: "A quelle heure le conducteur a-t-il pris son service ?",
            inputType: "time"
          },

          {
            type: "radiogroup",
            name: "Le mode de déplacement utilisé était-il conforme aux procédures habituelles ?",
            hasOther: true,
            choices: ["oui", "non"]
          },

          {
            type: "radiogroup",
            name: "Y avait-il un passager à bord ?",
            choices: ["oui", "non"]
          },

          {
            type: "radiogroup",
            name: "Le conducteur avait-il pris son service la veille de l'accident ?",
            hasOther: true,
            choices: ["oui", "non"]
          },

          {
            type: "dropdown",
            name: "Dans quelle situation se trouvait le conducteur par rapport à l'horaire prévu ?",
            hasOther: true,
            choices: ["dans les temps", "en avance", "en retard"]
          },

          {
            type: "text",
            name: "Quelle est la distance parcourue entre la prise de service et le lieu de l'accident ?",
            inputType: "number"
          },
          {
            type: "dropdown",
            name: "Dans les instants qui ont précédé l'accident, le conducteur était-il préoccupé ?  ",
            hasOther: true,
            choices: ["oui", "non"]
          },
          {
            type: "dropdown",
            name: "Dans les instants qui ont précédé l'accident, le conducteur était-il distrait ?  ",
            hasOther: true,
            choices: ["oui", "non"]
          },
          {
            type: "checkbox",
            name: "Dans les instants qui ont précédé l'accident, le conducteur se sentait-il plutôt ?  ",
            hasOther: true,
            choices: ["de bonne humeur", "de mauvaise humeur", "en pleine forme", "fatigué"]
          },
          {
            type: "text",
            name: "Autres remarques (le conducteur a-t-il remarqué quelque chose de particulier avant l'accident ?) :",
            inputType: "text"
          },

        ]

      },
      {//page 5
        name: "Circonstances véhicule entreprise",
        title: "Circonstances véhicule entreprise",
        questions: [{
          type: "dropdown",
          name: "Type de véhicule impliqué dans l'accident",
          hasOther: true,
          choices: ["véhicule de l'entreprise", "véhicule de location", "véhicule personnel"]
        },

        {
          type: "radiogroup",
          name: "Le véhicule est-il habituel au conducteur",
          choices: ["oui", "non"]
        },

        {
          type: "radiogroup",
          name: "Le véhicule était-il adapté à la mission effectuée ?",
          hasOther: true,
          choices: ["oui", "non"]
        },

        {
          type: "radiogroup",
          name: "Le chargement était-il :",
          choices: ["inférieur (ou égal) à la charge utile", "supérieur à la charge utile"]
        },

        {
          type: "text",
          name: "Quelle est la distance parcourue entre la prise de service et le lieu de l'accident ?",
          inputType: "number",
        },
        {
          type: "radiogroup",
          name: "Le véhicule était-il à l'arrêt ou en stationnement ?",
          hasOther: true,
          choices: ["oui", "non"]
        },
        //Si oui :
        {//activé si oui
          type: "dropdown",
          name: "Quel genre de stationnement ?",
          hasOther: true,
          choices: ["parking", "long du trottoir", "stationnement régulier"]
        },

        ]
      },


      {//page 6
        name: "Etat des lieux",
        title: "Etat des lieux",
        questions: [

          {
            type: "text",
            name: "Quel jour l'accident a-t-il eu lieu ?",
            inputType: "date",
            PlaceHolder: "jj/mm/aaaa hh:mm", //!!!! heure !!!!!
          },
          {
            type: "text",
            name: "A quelle heure l'accident a-t-il eu lieu ?",
            inputType: "time",
            PlaceHolder: "hh:mm",
          },

          //affichage jour de la semaine correspondant
          {
            type: "dropdown",
            name: "Indiquer le type de sinistre :",
            hasOther: true,
            choices: ["accident matériel", "accident corporel"]
          },
          {
            type: "checkbox",
            name: "Quelles étaient les conditions météo au moment de l'accident :",
            hasOther: true,
            choices: ["pluie", "temps sec", "brouillard", "neige", "verglas"]
          },
          {
            type: "radiogroup",
            name: "Quelles étaient les conditions de lumière au moment de l'accident :",
            hasOther: true,
            choices: ["aube", "journée", "crépuscule"]
          },
          {
            type: "dropdown",
            name: "Type d'environnement où l'accident s'est produit :",
            hasOther: true,
            choices: ["zone urbaine", "zone rurale",]
          },

          {
            type: "dropdown",
            name: "Précisez la configuration des lieux de l'accident :",
            hasOther: true,
            choices: ["ligne droite", "courbe",]
          },
          {// dessin
            type: "text",
            name: "Indiquer le type de collision",
            hasOther: true,
            choices: ["", "",]
          },
          {// dessin
            type: "text",
            name: "Indiquer le point de choc initial sur le véhicule",
            hasOther: true,
            choices: ["", "",]
          },
          {
            type: "text",
            name: "Indiquer la gravité des dommages du véhicule :",
            inputType: "text"
          },
          {
            type: "radiogroup",
            name: "Quelle est la responsabilité finale du véhicule de la flotte ?",
            hasOther: true,
            choices: ["100% tort", "50% tort", "0 tort"]
          },
          {
            type: "radiogroup",
            name: "Cet accident a-t-il été déclaré auprès de l'assurance ?",
            hasOther: true,
            choices: ["oui", "non"]
          },
        ]
      },

      {//page 7
        name: "Tiers",
        title: "Tiers",
        questions: [
          {
            type: "dropdown",
            name: "Indiquer le type de sinistre :",
            hasOther: true,
            choices: ["accident matériel", "accident corporel"]
          },
          {
            type: "dropdown",
            name: "Indiquer le type de tiers impliqué :",
            hasOther: true,
            choices: ["personne", "véhicule léger", "véhicule lourd", "2 roues non motorisé", "2 roues motorisé"]
          },
          {
            type: "checkbox",
            name: "Préciser les circonstances du véhicule tiers au moment de l'accident :",
            hasOther: true,
            choices: ["stationnement", "en circulation", "en marche arrière (sauf si mise à quai", "à l'arrêt", "quittait un stationnement"]
          },
        ]

      },

      {//page 8
        name: "Informations complémentaires",
        title: "Informations complémentaires",
        questions: [
          {
            type: "text",
            name: "Le conducteur estime sa responsabilité à :",
            inputType: "text"
          },
          {
            type: "text",
            name: "Précisions éventuelles sur les circonstances de l'accident :",
            inputType: "text"
          },
          {
            type: "radiogroup",
            name: "Pensez-vous que, dans de mêmes conditions, il soit possible d'éviter un tel accident ?",
            hasOther: true,
            choices: ["oui", "non"]
          },
          {
            type: "radiogroup",
            name: "Pensez-vous que le sinistre était évitable ?",
            hasOther: true,
            choices: ["oui", "non"]
          },
          {
            type: "text",
            name: "Que proposez-vous pour qu'un tel accident ne se reproduise pas ?",
            inputType: "text"
          },

        ]

      },
      {//page 9
        name: "Finalisatiion du rapport",
        title: "Finalisation du rapport",

      },

    ]

  };

  onSurveySaved(survey) {
    console.log('survey ',survey)
    this.json = survey;
    console.log('this.json ',this.json)

  }
}
